window.addEventListener("load", function () {

    const submitBtn = document.querySelector(".submit_btn");
    const pageForm = document.querySelector(".password-form");

    pageForm.addEventListener("click", showPassword)
    submitBtn.addEventListener("click", validateInputInfo);

    function validateInputInfo(evt) {
        showRemoveErrorMsg(pageForm, "remove");
        evt.preventDefault();
        const passwordField = document.querySelector(".password_main");
        const passwordConfirmField = document.querySelector(".password_confirm");
        if (!passwordField.value || !passwordConfirmField.value || (passwordField.value !== passwordConfirmField.value)) {
            showRemoveErrorMsg(evt.target);
        } else {
            alert("You are welcome");

        }
    }

    function showPassword(evt) {
        if (evt.target.tagName !== "I") {
            return;
        }
        const currentField = evt.target.previousElementSibling;
        if (currentField.getAttribute("type") === "text" && evt.target.classList.contains("fa-eye-slash")) {
            currentField.setAttribute("type", "password");
            evt.target.classList.replace("fa-eye-slash", "fa-eye");
        } else {
            currentField.setAttribute("type", "text");
            evt.target.classList.replace("fa-eye", "fa-eye-slash");
        }
    }

    function showRemoveErrorMsg(elemToInsertAfter, action = "show") {
        if (action === "show") {
            const errorMsg = document.createElement("span");
            errorMsg.classList.add("error_msg");
            errorMsg.innerText = "Нужно ввести одинаковые значения";
            elemToInsertAfter.before(errorMsg);
        }
        if (action === "remove") {
            for (const item of elemToInsertAfter.children) {
                if (item.className.includes("error_msg")) {
                    item.remove();
                }
            }
        }
    }


});
